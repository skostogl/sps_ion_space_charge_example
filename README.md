# SPS Ion Space Charge Example

This repository contains a first test-version example of space charge in the SPS, using modules provided by X-suite. 

The folder `SPS_sequence` contain Python wrappers and MAD-X scripts to generate an up-to-date version of SPS Q26 ion optics, from the [official CERN optics repository](https://acc-models.web.cern.ch/acc-models/sps/), whose repository can be cloned. The python wrapper `Generate_SPS_Q26_ion_sequence.py` calls the corresponding MAD-X file, which in turns extracts the needed files from the `acc-models` repository. To compare optics functions, the python wrapper also contains `acc_lib`, my personal collection of useful accelerator functions, including plotting tools and MAD-X error handling, whose repository is located [here](https://gitlab.cern.ch/elwaagaa/acc_lib). 

In file YYY (still to be added), the SPS ion sequence is loaded. We use **X-suite** (see [documentation](https://xsuite.readthedocs.io/en/latest/index.html)) to generate a line where space charge can be installed. 

**sps_ion_space_charge_example/IBS_module/**: IBS module from M. Zampetakis in https://github.com/MichZampetakis/IBS_for_Xsuite
**sps_ion_space_charge_example/SPS_sequence/SPS_2021_ions_injection.py**: example of xsuite line creation for ions

