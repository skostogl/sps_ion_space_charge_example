"""
Script to generate SPS ion lattice sequence file with `makethin` to ensure compatibility with Xsuite tracking 
For the ions, Q26 optics is used 
"""
import numpy as np

import xobjects as xo
import xtrack as xt
import xpart as xp
import matplotlib.pyplot as plt
import sys 
import os 
from cpymad.madx import Madx  # Import Cpymad module, and repositories with sequence files 

# personal collection of useful accelerator functions, including plotting tools and madx error handling
# gitlab repository at: https://gitlab.cern.ch/elwaagaa/acc_lib 
import acc_lib

# Flags
makethin = True

# Initiate MAD-X process for lattice file 
try: # remove temporary MADX file if exists since before
    os.remove('tempfile')
except OSError:
    pass

with open('tempfile', 'w') as f:
    madx = Madx(stdout=f,stderr=f)        
madx.option(echo=False, warn=True, info=False, debug=False, verbose=False)

# Activate the aperture for the Twiss flag to include it in Twiss command! 
madx.input('select,flag=twiss,clear;')
madx.select(flag='twiss', column=['N1', 'apertype', 'aper_1', 'aper_2', 'aper_3', 'aper_4', 'apoff_1', 'apoff_2', 'aptol_1', 'aptol_2', 'aptol_3'])
madx.call('Generate_SPS_Q26_ion_seq.madx')


#%% ###################### MAKE THIN FOR SPS SEQUENCE ################################################
if not makethin:
    # Save the SPS sequence into a file, without making it thin 
    madx.command.save(sequence='sps', file='SPS_Pb_ions.seq', beam=True)

if makethin:
    # To prepare sequence for X-suite, perform a makethin command. Check that chromaticities and tunes are not altered

    twiss1 = madx.twiss(sequence='sps',)
    print("Before MAKETHIN, Tunes and Chromaticity: q1={}, q2={}, dq1={}, dq2={}".format(
        twiss1.summary['q1'], 
        twiss1.summary['q2'],
        twiss1.summary['dq1'],
        twiss1.summary['dq2']    
        ))
    madx.command.select(flag='makethin', slice=1, thick=False)
    madx.command.makethin(sequence='sps', style='teapot', makedipedge=False)
    
    twiss2 = madx.twiss(sequence='sps',)
    print("After  MAKETHIN, Tunes and Chromaticity: q1={}, q2={}, dq1={}, dq2={}".format(
        twiss2.summary['q1'], 
        twiss2.summary['q2'],
        twiss2.summary['dq1'],
        twiss2.summary['dq2']    
        ))
    
    # Save the SPS sequence into a file to use with X-suite
    madx.command.save(sequence='sps', file='SPS_Pb_ions_thin.seq', beam=True)


# Transfer this sequence to X-suite 
madx.use(sequence='sps')

# Build Xtrack line importing MAD-X expressions
line = xt.Line.from_madx_sequence(madx.sequence['sps'],
                                  deferred_expressions=True,
                                  install_apertures=True
                                  )


# Test plotting the envelope and aperture 
fig = plt.figure(figsize=(10,7))
ax = acc_lib.madx_tools.plot_envelope(fig, madx, twiss2)
s, aper = acc_lib.madx_tools.get_apertures_real(twiss1)
acc_lib.madx_tools.plot_apertures_real(ax, s, aper)

fig2 = plt.figure(figsize=(10,7))
ax2 = acc_lib.madx_tools.plot_envelope(fig2, madx, twiss2, axis='vertical')
s, aper = acc_lib.madx_tools.get_apertures_real(twiss1,  axis='vertical')
acc_lib.madx_tools.plot_apertures_real(ax2, s, aper)

# If any madx error, uncomment following line or run in iPython terminal 
# acc_lib.madx_tools.print_madx_error()